//
//  Store+Helper.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/2/5.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

struct Store {
    static func createTablesIfNotExsists() throws {
        try SimpleStore.createIfNotExsists()
        try ChangeInfo.createIfNotExsists()
        try CommentInfo.createIfNotExsists()
    }

    static func cleanAll() throws {
        try ChangeInfo.deleteAll()
        try CommentInfo.deleteAll()
    }
}
