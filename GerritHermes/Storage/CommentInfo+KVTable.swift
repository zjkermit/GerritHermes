//
//  CommentInfo+KVTable.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/2/7.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

extension CommentInfo: KVTable {
    static var tableName: String { "comments" }

    var keyId: String { id }
}
