//
//  ChangeInfo+KVTable.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/2/7.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

extension ChangeInfo: KVTable {
    static var tableName: String { "changes" }

    var keyId: String { changeID }
    var relatedKeyId: String? { project }
}
