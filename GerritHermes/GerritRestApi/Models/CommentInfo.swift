//
//  CommentInfo.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/9.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

struct CommentInfo: Codable {
    enum Side: String, Codable {
        case revision = "REVISION"
        case parent = "PARENT"
    }

    let id: String
    let patchSet: Int?
    var path: String?
    let side: Side?
    let parent: String?
    let line: Int?
    let range: CommentRange?
    let inReplyTo: String?
    let message: String?
    let updated: Date
    let author: AccountInfo?
    let unresolved: Bool?

    enum CodingKeys: String, CodingKey {
        case id
        case patchSet = "patch_set"
        case path
        case side
        case parent
        case line
        case range
        case inReplyTo = "in_reply_to"
        case message
        case updated
        case author
        case unresolved
    }
}

struct CommentRange: Codable {
    /// 1-based
    let startLine: Int
    /// 0-based
    let startCharacter: Int
    /// 1-based
    let endLine: Int
    /// 0-based
    let endCharacter: Int

    enum CodingKeys: String, CodingKey {
        case startLine = "start_line"
        case startCharacter = "start_character"
        case endLine = "end_line"
        case endCharacter = "end_character"
    }
}
