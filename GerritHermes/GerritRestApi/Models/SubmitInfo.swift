//
//  SubmitInfo.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/3/21.
//  Copyright © 2020 周剑. All rights reserved.
//

import Foundation

struct SubmitInfo: Decodable {
    let status: String
}
