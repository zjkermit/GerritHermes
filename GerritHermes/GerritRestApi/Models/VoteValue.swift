//
//  VoteValue.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/9.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

enum VoteValue: String, Codable, CaseIterable {
    enum ValueError: Error {
        case valueOutOfRange
    }

    case minus2 = "-2"
    case minus1 = "-1"
    case zero = " 0"
    case plus1 = "+1"
    case plus2 = "+2"

    init(_ value: Int) throws {
        switch value {
        case -2: self = .minus2
        case -1: self = .minus1
        case 0: self = .zero
        case 1: self = .plus1
        case 2: self = .plus2
        default:
            throw ValueError.valueOutOfRange
        }
    }
}
