//
//  Requirement.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/9.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

struct Requirement: Codable {
    enum Status: String, Codable {
        case ok = "OK"
        case notReady = "NOT_READY"
        case ruleError = "RULE_ERROR"
    }

    let status: Status
    let fallbackText: String
    let type: String
    let data: [String: String]?
}
