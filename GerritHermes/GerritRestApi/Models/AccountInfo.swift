//
//  AccountInfo.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/1.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

// MARK: - Owner
// GET /accounts/self HTTP/1.0
struct AccountInfo: Codable {
    let accountID: Int
    let name: String?
    let email: String?
    let username: String?
    let avatars: [AvatarInfo]?
    let status: String?
    let inactive: Bool?

    private enum CodingKeys: String, CodingKey {
        case accountID = "_account_id"
        case name
        case email
        case username
        case avatars
        case status
        case inactive
    }

}
