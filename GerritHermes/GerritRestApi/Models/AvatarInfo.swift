//
//  AvatarInfo.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/9.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

struct AvatarInfo: Codable {
    let url: String
    let height: Float
    let width: Float
}
