//
//  ChangeMessageInfo.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/9.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

struct ChangeMessageInfo: Codable {
    let id: String
    let tag: String?
    let author: AccountInfo?
    let realAuthor: AccountInfo?
    let date: Date
    let message: String
    let revisionNumber: Int?

    enum CodingKeys: String, CodingKey {
        case id, author, date, message
        case tag
        case revisionNumber = "_revision_number"
        case realAuthor = "real_author"
    }
}
