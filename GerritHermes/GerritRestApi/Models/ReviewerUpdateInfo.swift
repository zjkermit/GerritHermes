//
//  ReviewerUpdateInfo.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/9.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

struct ReviewerUpdateInfo: Codable {
    enum State: String, Codable {
        case reviewer = "REVIEWER"
        case cc = "CC"
        case removed = "REMOVED"
    }

    let updated: Date
    let updatedBy: AccountInfo
    let reviewer: AccountInfo
    let state: State

    private enum CodingKeys: String, CodingKey {
        case updated
        case updatedBy = "updated_by"
        case reviewer
        case state
    }
}
