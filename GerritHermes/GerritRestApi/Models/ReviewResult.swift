//
//  ReviewResult.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/3/18.
//  Copyright © 2020 周剑. All rights reserved.
//

import Foundation

struct ReviewResult: Decodable {
    struct Result: Decodable {
        let codeReview: Int

        enum CodingKeys: String, CodingKey {
            case codeReview = "Code-Review"
        }
    }
    let labels: Result
}
