//
//  AccountInfo+Sender.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/19.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

extension AccountInfo {
    var sender: String {
        return name ?? email ?? "ID为\(accountID)的用户"
    }
}
