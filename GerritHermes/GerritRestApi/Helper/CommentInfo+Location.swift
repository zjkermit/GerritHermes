//
//  CommentInfo+Location.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/21.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

extension CommentInfo {
    var location: String {
        var result = ""
        if let path = self.path {
            result += "\"\(path)\""
            if let line = self.line {
                result += "\n[Line \(line)]:"
            } else if let range = self.range {
                result += "\n<Line \(range.startLine) to \(range.endLine)>:"
            }
        }
        return result
    }

    var gerritPath: String {
        var result = ""
        if let patchSet = self.patchSet, let path = self.path {
            result += "/\(patchSet)/\(path)"
            if let line = self.line {
                result += "#\(line)"
            }
        }
        return result
    }
}
