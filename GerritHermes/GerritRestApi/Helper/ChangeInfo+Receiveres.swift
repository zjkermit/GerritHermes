//
//  ChangeInfo+Receiveres.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/20.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

extension ChangeInfo {
    /// Include reviewers CC and assignee
    var allReceivers: [AccountInfo] {
        var receivers = reviewers?.reviewer ?? []
        receivers.append(contentsOf: reviewers?.cc ?? [])
        if let assignee = self.assignee {
            receivers.append(assignee)
        }
        return receivers.filter { $0 != owner }
    }
}
