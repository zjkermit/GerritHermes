//
//  VoteValue+Message.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/20.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

extension VoteValue {
    var message: String {
        switch self {
        case .minus2: return "Code-Review -2"
        case .minus1: return "Code-Review -1"
        case .plus1: return "Code-Review +1"
        case .plus2: return "Code-Review +2"
        default:
            return ""
        }
    }
}
