//
//  AccountInfo+Equetable.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/20.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

extension AccountInfo: Equatable {
    static func == (lhs: Self, rhs: Self) -> Bool {
        lhs.accountID == rhs.accountID
    }
}
