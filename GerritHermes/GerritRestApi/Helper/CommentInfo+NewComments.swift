//
//  CommentInfo+NewComments.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/2/7.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

extension CommentInfo {
    static func newComments(old: [CommentInfo], all: [CommentInfo]) -> [CommentInfo] {
        guard old.count < all.count else { return [] }
        let sorter = { (comment1: CommentInfo, comment2: CommentInfo) -> Bool in
            return comment1.updated.compare(comment2.updated) == .orderedAscending
        }
        let allComments = all.sorted(by: sorter)

        let newComments: [CommentInfo] = Array(allComments[old.count...])
        return newComments
    }
}
