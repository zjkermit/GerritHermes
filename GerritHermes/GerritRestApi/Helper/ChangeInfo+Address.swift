//
//  ChangeInfo+Address.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/19.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

extension ChangeInfo {
    var gerritAddress: String {
        return "\(GeneralModel.shared.gerritAddress)/c/\(project)/+/\(number)"
    }
}
