//
//  ChangeInfo+Equetable.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/20.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

extension ChangeInfo: Equatable {
    static func == (lhs: Self, rhs: Self) -> Bool {
        lhs.changeID == rhs.changeID
    }
}
