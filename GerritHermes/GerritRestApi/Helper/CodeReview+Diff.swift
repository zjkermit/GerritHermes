//
//  CodeReview+Diff.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/20.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

extension CodeReview {
    typealias VoteInfo = (account: AccountInfo, vote: VoteValue)

    var voteInfo: VoteInfo? {
        if let account = rejected {
            return (account, .minus2)
        }
        if let account = disliked {
            return (account, .minus1)
        }
        if let account = recommended {
            return (account, .plus1)
        }
        if let account = approved {
            return (account, .plus2)
        }
        return nil
    }

    static func diff(old: CodeReview?, new: CodeReview?) -> VoteInfo? {
        guard let new = new, let newVote = new.voteInfo else { return nil }
        if let oldVote = old?.voteInfo {
            if oldVote.account.accountID != newVote.account.accountID {
                return newVote
            }
            if oldVote.vote != newVote.vote {
                return newVote
            }
            return nil
        } else {
            return newVote
        }
    }
}
