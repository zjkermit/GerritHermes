//
//  Split+VennDiagram.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/20.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

extension Array {
    typealias SplitResult = (remaining: [Element], common: [(Element, Element)], additional: [Element])

    func split(new: [Element], equal: (Element, Element) -> Bool) -> SplitResult {
        var remaining = self
        var common = [(Element, Element)]()
        var additional = new

        for index in (0..<new.count).reversed() {
            let n = new[index]
            if let idx = remaining.firstIndex(where: { equal($0, n) }) {
                additional.remove(at: index)
                let o = remaining.remove(at: idx)
                common.append((o, n))
            }
        }

        return (remaining, common, additional)
    }

}
