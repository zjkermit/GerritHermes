//
//  ChangeInfo+MessageTitle.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/21.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

extension ChangeInfo {
    var messageTitle: String {
        return """
        🚀 "\(self.subject)"
        [🏰 \(project)] [👤 \(owner.sender)] \(topic == nil ? "" : "[🔖 \(topic!)]")
        """
    }
}
