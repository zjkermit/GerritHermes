//
//  ChangeInfo+Overtime.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/21.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

extension ChangeInfo {
    var isNewChange: Bool {
        let overtimeDate = created.addingTimeInterval(OvertimeInterval)
        // 修正时间和服务器保持一致
        let currentDate = Date().addingTimeInterval(60 * 60 * 8)
        return overtimeDate > currentDate
    }
}
