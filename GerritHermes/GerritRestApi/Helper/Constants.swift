//
//  Constants.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/6/30.
//  Copyright © 2020 周剑. All rights reserved.
//

import Foundation

let MinTimeInterval: TimeInterval = 60
let MaxTimeInterval: TimeInterval = 60 * 10
let OvertimeInterval: TimeInterval = MaxTimeInterval + 10
