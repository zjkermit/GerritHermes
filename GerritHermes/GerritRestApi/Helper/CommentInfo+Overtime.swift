//
//  CommentInfo+Overtime.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/2/10.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

extension CommentInfo {
    var isNewComment: Bool {
        let overtimeDate = updated.addingTimeInterval(OvertimeInterval)
        // 修正时间和服务器保持一致
        let currentDate = Date().addingTimeInterval(60 * 60 * 8)
        return overtimeDate > currentDate
    }
}
