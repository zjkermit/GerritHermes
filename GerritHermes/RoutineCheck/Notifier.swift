//
//  Notifier.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/17.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation
import Alamofire

final class Notifier {
    static func notify(wxUsers: [String], message: String) {
        notify(url: "https://oa.zhenguanyu.com/weixin/", parameters: ["touser": wxUsers, "toparty": [String](), "content": message])
    }

    static func notify(webhookUrl: String, message: String) {
        notify(url: webhookUrl, parameters: ["msgtype": "text", "text": ["content": message]])
    }

    private static func notify(url: String, parameters: [String: Any]) {
        Alamofire
            .request(url,
                     method: .post,
                     parameters: parameters,
                     encoding: JSONEncoding.default,
                     headers: ["content-type": "application/json"])
            .responseData { (response) in
                switch response.result {
                case .success(let data):
                    Log.info("Post to \(url), respond data size: \(data.count)")
                case .failure(let error):
                    Log.error("Post to \(url), receive error: \(error.localizedDescription)")
                }
            }
    }
}
