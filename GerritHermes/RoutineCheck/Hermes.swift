//
//  Hermes.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/12.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation

final class Hermes {
    static func send(to accounts: [AccountInfo], _ messages: String...) {
        let separator = "\n------------------------------------------------\n"
        let message = messages.compactMap { $0 }.joined(separator: separator)
        let users = accounts.compactMap { $0.username }

        #if DEBUG
        switch DebugModel.shared.mode {
        case .developerOnly:
            let prefix1 = "😈【应该发送给: \(users.joined(separator: ","))】"
            send(to: [DebugModel.shared.developer], message: prefix1 + "\n" + String(describing: message))
        case .unlimitedTest:
            send(to: users.filter { $0 != DebugModel.shared.developer }, message: message)

            let prefix1 = "😈【会发送给: \(users.joined(separator: ","))】"
            send(to: [DebugModel.shared.developer], message: prefix1 + "\n" + String(describing: message))
        case .release:
            send(to: users, message: message)
        }
        #else
        send(to: users, message: message)
        #endif
    }

    static func send(to wxUsers: [String], message: String) {
        Notifier.notify(wxUsers: wxUsers, message: message)
    }

}
