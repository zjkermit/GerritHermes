//
//  CompletionHandler.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/2/6.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation
import Combine
import SwiftUI

final class CompletionHandler: ObservableObject {
    static let shared = CompletionHandler()
    @Published var errorCount = 0

    static func handle(_ completion: Subscribers.Completion<GerritError>, context: String, file: String = #file, function: String = #function, line: Int = #line) {
        if let error = completion.failure {
            handle(error, context: context, file: file, function: function, line: line)
        } else {
            shared.errorCount = 0
            Log.info(context, file: file, function: function, line: line)
        }
    }

    static func handle(_ error: Error, context: String, file: String = #file, function: String = #function, line: Int = #line) {
        let message = context + ": " + error.localizedDescription
        Log.error(message, file: file, function: function, line: line)
        shared.errorCount += 1
    }
}
