//
//  LocalNotification.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/2/6.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation
import Cocoa
import UserNotifications

final class LocalNotification: NSObject {
    private static let shared = LocalNotification()

    static func send(title: String, body: String, image: NSImage? = nil) {
        let notification = NSUserNotification()
        NSUserNotificationCenter.default.delegate = shared
        notification.title = title
        notification.informativeText = body
        notification.contentImage = image
        NSUserNotificationCenter.default.deliver(notification)
        Log.info("Send local notification", context: notification)
    }
}

extension LocalNotification: NSUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: NSUserNotificationCenter, shouldPresent notification: NSUserNotification) -> Bool {
        return true
    }
}

