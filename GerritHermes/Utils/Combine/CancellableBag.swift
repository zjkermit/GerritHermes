//
//  CancellableBag.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/8.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Combine

final class CancellableBag {
    private var bag = Set<AnyCancellable>()

    deinit {
        for cancellable in bag {
            cancellable.cancel()
        }
    }

    func insert(cancellable: AnyCancellable) {
        bag.insert(cancellable)
    }
}

extension AnyCancellable {
    func cancel(by bag: CancellableBag) {
        bag.insert(cancellable: self)
    }
}
