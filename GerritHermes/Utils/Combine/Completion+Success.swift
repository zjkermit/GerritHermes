//
//  Completion+Success.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/12.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Combine

extension Subscribers.Completion {

    var isSuccess: Bool {
        if case .finished = self {
            return true
        }
        return false
    }

    var failure: Failure? {
        if case let .failure(failure) = self {
            return failure
        }
        return nil
    }

}
