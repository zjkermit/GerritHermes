//
//  Publishers+MapReplace.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/10.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation
import Combine

extension Publisher {
    func mapReplace<T>(value: T) -> Publishers.Map<Self, T> {
        return Publishers.Map(upstream: self) { _ in value }
    }
}
