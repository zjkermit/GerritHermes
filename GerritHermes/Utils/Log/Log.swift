//
//  Log.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/15.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import SwiftyBeaver

final class Log {
    private static var log: SwiftyBeaver.Type = {
        let log = SwiftyBeaver.self

        let console = ConsoleDestination()
        log.addDestination(console)
        return log
    }()

    static func verbose(_ message: @autoclosure () -> String,
        file: String = #file, function: String = #function, line: Int = #line, context: Any? = nil) {
        log.custom(level: .verbose, message: message(), file: file, function: function, line: line, context: context)
    }

    static func debug(_ message: @autoclosure () -> String,
        file: String = #file, function: String = #function, line: Int = #line, context: Any? = nil) {
        log.custom(level: .debug, message: message(), file: file, function: function, line: line, context: context)
    }

    static func info(_ message: @autoclosure () -> String,
        file: String = #file, function: String = #function, line: Int = #line, context: Any? = nil) {
        log.custom(level: .info, message: message(), file: file, function: function, line: line, context: context)
    }

    static func warning(_ message: @autoclosure () -> String,
        file: String = #file, function: String = #function, line: Int = #line, context: Any? = nil) {
        log.custom(level: .warning, message: message(), file: file, function: function, line: line, context: context)
    }
    
    static func error(_ message: @autoclosure () -> String,
        file: String = #file, function: String = #function, line: Int = #line, context: Any? = nil) {
        log.custom(level: .error, message: message(), file: file, function: function, line: line, context: context)
    }
}
