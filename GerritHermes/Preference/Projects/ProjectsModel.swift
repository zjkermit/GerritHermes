//
//  ProjectsModel.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/2/10.
//  Copyright © 2020 周剑. All rights reserved.
//

import Foundation
import SwiftUI

private extension String {
    static let projects = "projects"
}

final class ProjectsModel: ObservableObject {
    static let shared = ProjectsModel()

    private let bag = CancellableBag()
    @Published var projects: [String] = []

    private init() {
        do {
            projects = try SimpleStore.object(for: .projects) ?? []
        } catch let error {
            Log.error("读取project列表失败", context: error)
        }

        $projects.sink { (projects) in
            do {
                try SimpleStore.set(projects, for: .projects)
            } catch let error {
                Log.error("写project列表失败", context: error)
            }
        }.cancel(by: bag)
    }

}
