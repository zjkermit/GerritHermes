//
//  ProjectsViewController.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/1/13.
//  Copyright © 2020 J.Zhou. All rights reserved.
//

import Foundation
import Preferences
import SwiftUI
import SnapKit

extension PreferencePane.Identifier {
    static let projects = Identifier("projects")
}

final class ProjectsViewController: NSViewController, PreferencePane {
    let preferencePaneIdentifier = PreferencePane.Identifier.projects
    var preferencePaneTitle = "Projects"
    let toolbarItemIcon = NSImage(named: NSImage.userAccountsName)!

    override func loadView() {
        self.view = NSView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        let configView = NSHostingView(rootView: ProjectsView(selectedIndex: -1))
        view.addSubview(configView)
        configView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }

}
