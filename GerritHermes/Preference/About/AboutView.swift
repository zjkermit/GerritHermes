//
//  AboutView.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/2/10.
//  Copyright © 2020 周剑. All rights reserved.
//

import SwiftUI

struct AboutView: View {
    var body: some View {
        VStack {
            Image("Icon@64")
                .frame(width: 60, height: 60)

            Text("Gerrit Hermes")
                .font(.headline)

            Text("Version 0.1.0")
                .font(.footnote)
                .padding(.bottom)

            Text("Copyright © 2020 周剑.\nAll rights reserved. License GPLv3.")
                .multilineTextAlignment(.center)
                .font(.footnote)
        }.padding()
    }
}
