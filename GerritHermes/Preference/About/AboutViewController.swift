//
//  AboutViewController.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/2/10.
//  Copyright © 2020 周剑. All rights reserved.
//

import Foundation
import Preferences
import SwiftUI
import SnapKit

extension PreferencePane.Identifier {
    static let about = Identifier("about")
}

final class AboutViewController: NSViewController, PreferencePane {
    let preferencePaneIdentifier = PreferencePane.Identifier.about
    var preferencePaneTitle = "About"
    let toolbarItemIcon = NSImage(named: NSImage.infoName)!

    override func loadView() {
        self.view = NSView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        let aboutView = NSHostingView(rootView: AboutView())
        view.addSubview(aboutView)
        aboutView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
}
