//
//  DebugModel.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/2/10.
//  Copyright © 2020 周剑. All rights reserved.
//

#if DEBUG

import SwiftUI

private extension String {
    static let developer = "developer"
}

final class DebugModel: ObservableObject {
    enum DebugMode: CustomStringConvertible {
        case developerOnly
        case unlimitedTest
        case release

        var description: String {
            switch self {
            case .developerOnly:
                return "仅发送给开发者"
            case .unlimitedTest:
                return "线上模式(抄送开发者)"
            case .release:
                return "线上模式"
            }
        }

    }

    static let shared = DebugModel()

    private let bag = CancellableBag()
    @Published var mode = DebugMode.developerOnly
    @Published var developer = ""

    init() {
       do {
            developer = try SimpleStore.object(for: .developer) ?? ""
        } catch let error {
            Log.error("读取developer失败", context: error)
        }

        $developer.sink { (developer) in
            do {
                try SimpleStore.set(developer, for: .developer)
            } catch let error {
                Log.error("写developer失败", context: error)
            }
        }.cancel(by: bag)

        objectWillChange
            .delay(for: .milliseconds(300), scheduler: DispatchQueue.main)
            .sink {
                LocalNotification.send(title: "当前模式",
                                       body: DebugModel.shared.mode.description)
            }.cancel(by: bag)
    }
}

#endif
