//
//  DebugView.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/2/10.
//  Copyright © 2020 周剑. All rights reserved.
//

#if DEBUG

import SwiftUI

struct DebugView: View {
    @ObservedObject private var model = DebugModel.shared

    var body: some View {
        VStack {
            Text("当前模式：\(String(describing: model.mode))")

            HStack {
                Button(action: {
                    self.model.mode = .developerOnly
                }) {
                    Text("调试模式")
                }

                Button(action: {
                    self.model.mode = .unlimitedTest
                }) {
                    Text("预上线模式")
                }

                Button(action: {
                    self.model.mode = .release
                }) {
                    Text("线上模式")
                }
            }

            HStack {
                Text("Set Developer:")
                TextField("Developer Account", text: $model.developer)
            }

            Button(action: {
                Log.info("清除所有存储的消息")
                do {
                    try Store.cleanAll()
                } catch let error {
                    Log.error("清除数据库数据失败", context: error)
                }
            }) {
                Text("Clean All Data")
            }
        }.padding()
        .frame(width: 380)
    }
}

#endif
