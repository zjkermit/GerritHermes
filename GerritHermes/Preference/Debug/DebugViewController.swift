//
//  DebugViewController.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/2/10.
//  Copyright © 2020 周剑. All rights reserved.
//

#if DEBUG

import Foundation
import Preferences
import SwiftUI
import SnapKit

extension PreferencePane.Identifier {
    static let debug = Identifier("debug")
}

final class DebugViewController: NSViewController, PreferencePane {
    let preferencePaneIdentifier = PreferencePane.Identifier.debug
    var preferencePaneTitle = "Debug"
    let toolbarItemIcon = NSImage(named: NSImage.everyoneName)!

    override func loadView() {
        self.view = NSView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        let debugView = NSHostingView(rootView: DebugView())
        view.addSubview(debugView)
        debugView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
}

#endif
