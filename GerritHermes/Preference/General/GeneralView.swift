//
//  GeneralView.swift
//  GerritHermes
//
//  Created by J.Zhou on 2019/12/30.
//  Copyright © 2019 J.Zhou. All rights reserved.
//

import SwiftUI

struct GeneralView: View {
    @ObservedObject
    private var manager = GeneralModel.shared
    @ObservedObject
    private var completionHandler = CompletionHandler.shared
    @State
    private var textFieldWidth: CGFloat = .infinity
    private var someNumberProxy: Binding<String> {
        return Binding<String>(
            get: { "\(Int(self.manager.interval))" },
            set: {
                if let value = NumberFormatter().number(from: $0) {
                    self.manager.interval = value.doubleValue
                }
            }
        )
    }

    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Text("Gerrit")
                    .font(.headline)
                Spacer()
                Text("\(completionHandler.errorCount > 0 ? "🔴 Request Error" : "🟢 In Service")")
                    .font(.caption)
            }

            HStack {
                Text("Address: ")
                Spacer()
                TextField("Username", text: $manager.gerritAddress)
                    .frame(maxWidth: textFieldWidth)
                    .background(TextFieldWidthGetter())
            }
            HStack {
                Text("Username: ")
                Spacer()
                TextField("Username", text: $manager.username)
                    .frame(maxWidth: textFieldWidth)
                    .background(TextFieldWidthGetter())
            }
            HStack {
                Text("Credential: ")
                Spacer()
                SecureField("HTTP Credential", text: $manager.credential)
                    .frame(maxWidth: textFieldWidth)
                    .background(TextFieldWidthGetter())
            }
            HStack {
                Text("Interval: ")
                Spacer()
                TextField("Request Time Interval (seconds)", value: $manager.interval, formatter: NumberFormatter())
                    .frame(maxWidth: textFieldWidth)
                    .background(TextFieldWidthGetter())
            }

            HStack(alignment: .center) {
                Spacer()
                Button(action: { NSApp.terminate(nil) }) {
                    Text("Quit")
                }
                Spacer()
            }

            Spacer()
        }
        .padding()
        .frame(width: 350, height: 200)
        .onPreferenceChange(TextFieldPreferenceKey.self) { (width) in
            self.textFieldWidth = width
        }
    }
}

private struct TextFieldPreferenceKey: PreferenceKey {
    static var defaultValue: CGFloat = 0

    static func reduce(value: inout CGFloat, nextValue: () -> CGFloat) {
        if nextValue() > 0 {
            value = min(value, nextValue())
        }
    }
}

private struct TextFieldWidthGetter: View {
    var body: some View {
        GeometryReader { proxy in
            Rectangle()
                .fill(Color.clear)
                .preference(key: TextFieldPreferenceKey.self, value: proxy.size.width)
        }
    }
}
