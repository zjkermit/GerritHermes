//
//  GeneralViewController.swift
//  GerritHermes
//
//  Created by J.Zhou on 2019/12/30.
//  Copyright © 2019 J.Zhou. All rights reserved.
//

import Cocoa
import Preferences
import SwiftUI
import SnapKit

extension PreferencePane.Identifier {
    static let general = Identifier("general")
}

final class GeneralViewController: NSViewController, PreferencePane {
    let preferencePaneIdentifier = PreferencePane.Identifier.general
    var preferencePaneTitle = "General"
    let toolbarItemIcon = NSImage(named: NSImage.preferencesGeneralName)!

    override func loadView() {
        self.view = NSView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        let generalView = NSHostingView(rootView: GeneralView().environmentObject(GeneralModel.shared))
        view.addSubview(generalView)
        generalView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
}
