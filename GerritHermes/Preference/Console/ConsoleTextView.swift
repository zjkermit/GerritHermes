//
//  ConsoleTextView.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/3/15.
//  Copyright © 2020 周剑. All rights reserved.
//

import SwiftUI

struct ConsoleTextView: NSViewRepresentable {
    func makeNSView(context: Context) -> NSTextView {
        TinyConsole.shared.textView
    }

    func updateNSView(_ textView: NSTextView, context: Context) {
    }
}

struct ConsoleTextView_Previews: PreviewProvider {
    static var previews: some View {
        ConsoleTextView()
    }
}
