//
//  ConsoleView.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/3/15.
//  Copyright © 2020 周剑. All rights reserved.
//

import SwiftUI

struct ConsoleView: View {
    var body: some View {
       VStack {
            Text("Hermes Logs")
                .font(.headline)

            ConsoleTextView()
        }.padding()
        .frame(width: 500, height: 350)
    }
}

struct ConsoleView_Previews: PreviewProvider {
    static var previews: some View {
        ConsoleView()
    }
}
