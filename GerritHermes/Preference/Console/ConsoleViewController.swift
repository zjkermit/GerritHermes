//
//  ConsoleViewController.swift
//  GerritHermes
//
//  Created by J.Zhou on 2020/3/15.
//  Copyright © 2020 周剑. All rights reserved.
//

import Foundation
import Preferences
import SwiftUI
import SnapKit

extension PreferencePane.Identifier {
    static let console = Identifier("console")
}

final class ConsoleViewController: NSViewController, PreferencePane {
    let preferencePaneIdentifier = PreferencePane.Identifier.console
    var preferencePaneTitle = "Console"
    let toolbarItemIcon = NSImage(named: NSImage.menuOnStateTemplateName)!

    override func loadView() {
        self.view = NSView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        let consoleView = NSHostingView(rootView: ConsoleView())
        view.addSubview(consoleView)
        consoleView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
}

