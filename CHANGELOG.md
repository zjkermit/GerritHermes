# CHANGELOG

## 1.1.0

* 增加指令前缀处理逻辑：[WIP]、[TEST]、[AR]、[AM]

## 1.0.0

* 新Review到达提醒
* Subject冲突提醒
* Vote变化提醒
* Reviewer、CC、Assignee变动提醒
* Reply消息提醒
* Comment消息提醒
* Merge/Abandon提醒

