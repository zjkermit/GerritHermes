# GerritHermes

Retrieve changes from gerrit projects, and send notifications to WeChatWork users. (YTK only)

GerritHermes面向团队开发，每个团队可以自由配置多个仓库，之后仓库中的所有消息都将及时通过企业微信通知服务同步到相关用户。

GerritHermes使用Swift5.0开发，同时用到了Combine和SwiftUI，最低支持的OSX版本为10.15

> 赫尔墨斯（Hermes）取自希腊神话宙斯与迈亚的儿子，赫耳墨斯是信使之神，为宙斯传达消息。

## 核心逻辑

采用轮询的方式：

- 拉取指定仓库的changes，通过与数据库中存储的changes进行比较，拆分成三个集合：past、now和future
    - past: 存于本地记录且已经merge或abandone的change
    - now: 存于本地记录且gerrit中处理open状态的change
    - future: 本地数据中没有的change
- past集合处理逻辑
    - 如果所有者做了merge，且除了owner没有其他人vote，则通知reviewers是owner自己合入的代码
    - 如果非所有者做了merge，则通知所有者'谁'merge了代码
    - 如果被abandone了，则通知reviewers
- now集合处理逻辑
    - 如果之前是mergable，而现在不是，则通知所有者处理冲突
    - 处理前缀指令
    - diff reviewers，通知新增的reviewer、CC和assignee
    - 从messages中提取评论信息，发出新message通知
    - 遍历每一条未处理的comments，如果是评论则通知所有者，如果是回复则通知评论作者和所有者
- future集合处理逻辑
    - 如果创建时间未超时，则通知reviewers有新的review到达
    - 通知用户新的messages到达
    - 通知用户新的comments到达
    - 如果冲突则通知所有者解决冲突
    - 从labels中提取vote信息，通知用户vote的变化
    - 处理前缀指令

## 目前完成功能

* 新Review到达提醒
* Subject冲突提醒
* Vote变化提醒
* Reviewer、CC、Assignee变动提醒
* Reply消息提醒
* Comment消息提醒
* Merge/Abandon提醒
* 前缀指令处理，目前支持前缀指令：
    * 静默处理指令，不发送review信息给reviewers，comments会照常通知，包括：[WIP], [TEST]
    * 自动Rebase指令：[AR]
    * 自动+2并Merge指令：[AM]



