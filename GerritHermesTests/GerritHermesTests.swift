//
//  GerritHermesTests.swift
//  GerritHermesTests
//
//  Created by J.Zhou on 2020/2/10.
//  Copyright © 2020 周剑. All rights reserved.
//

import XCTest
@testable import GerritHermes

class GerritHermesTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testGrep() {
        let subject = "[AR] Bundle version"
        let result1 = subject.grep(pattern: "^\\[")
        let result2 = subject.grep(pattern: "^\\[.*\\]")
        let result3 = subject.grep(pattern: "^\\[(.*)\\]")
        XCTAssert(result1.isMatched)
        XCTAssert(result2.isMatched)
        XCTAssert(result3.isMatched)
        XCTAssert(result3.captures[1] == "AR")
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
